<?php

/**
 * @file
 * Commerce PSA module.
 */

/**
 * Implements hook_module_implements_alter().
 */
function commerce_psa_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'entity_info_alter') {
    // Need to put our hook lower in order to change
    // inline_form_entity controller.
    unset($implementations['commerce_psa']);
    $implementations['commerce_psa'] = 0;
  }
  if ($hook == 'form_alter') {
    // Need to do our changes earlier while altering form.
    unset($implementations['commerce_psa']);
    $implementations = array('commerce_psa' => 0) + $implementations;
  }
}

/**
 * Implements hook_views_api().
 */
function commerce_psa_views_api() {
  return array(
    'api' => '3.0',
    'path' => drupal_get_path('module', 'commerce_psa') . '/includes/views',
  );
}

/**
 * Implements hook_permission().
 */
function commerce_psa_permission() {
  return array(
    'manage commerce psa' => array(
      'title' => t('Manage Commerce Product Specific Attributes'),
      'description' => t('Allows to access Product Specific Attributes tab on product display page.'),
    ),
  );
}

/**
 * Access callback to edit line item inside `edit options` popup form.
 */
function commerce_psa_line_item_edit_access($ajax, $line_item) {
  global $user;
  if (!empty($line_item->order_id) && module_exists('commerce_order')) {
    $order = commerce_order_load($line_item->order_id);
    if ($order->uid == $user->uid && ($order->status == 'cart' || strpos($order->status, 'checkout') === 0)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Access callback to access PSA tab on node.
 */
function commerce_psa_attributes_form_access($node) {
  $types = array_keys(commerce_product_reference_node_types());
  return user_access('manage commerce psa') && in_array($node->type, $types);
}

/**
 * Implements hook_menu().
 */
function commerce_psa_menu() {
  $items['node/%node/psa/%commerce_psa'] = array(
    'title' => 'Attribute Options',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_psa_option_form', 1, 3),
    'access arguments' => array(1),
    'access callback' => 'commerce_psa_attributes_form_access',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'includes/commerce_psa.admin.inc',
    'weight' => 2,
  );

  $items['node/%node/psa'] = array(
    'title' => 'Product Attributes',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_psa_attribute_form', 1),
    'access arguments' => array(1),
    'access callback' => 'commerce_psa_attributes_form_access',
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/commerce_psa.admin.inc',
    'weight' => 2,
  );

  $items['psa/nojs/%commerce_line_item'] = array(
    'title' => 'Edit Options',
    'page callback' => 'commerce_psa_edit_options',
    'page arguments' => array(1, 2),
    'access arguments' => array(1, 2),
    'access callback' => 'commerce_psa_line_item_edit_access',
    'type' => MENU_CALLBACK,
    'file' => 'includes/commerce_psa.pages.inc',
  );

  $items['psa/ajax/%commerce_line_item'] = array(
    'delivery callback' => 'ajax_deliver',
  ) + $items['psa/nojs/%commerce_line_item'];

  return $items;
}

/**
 * Implements hook_theme().
 */
function commerce_psa_theme($existing, $type, $theme, $path) {
  return array(
    'psa_table' => array(
      'render element' => 'element',
      'file' => 'includes/commerce_psa.theme.inc',
    ),
    'psa_edit_options' => array(
      'variables' => array(
        'form' => NULL,
      ),
      'template' => 'templates/edit-options',
    ),
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function commerce_psa_theme_registry_alter(&$theme_registry) {
  // Swap inline_entity_form widget theme function with ours.
  $theme_registry['inline_entity_form_entity_table']['includes'][] = drupal_get_path('module', 'commerce_psa') . '/includes/commerce_psa.theme.inc';
  $theme_registry['inline_entity_form_entity_table']['function'] = 'commerce_psa_inline_entity_form_entity_table';
}

/**
 * Implements hook_field_widget_form_alter().
 */
function commerce_psa_field_widget_form_alter(&$element, &$form_state, $context) {
  if (!isset($element['#field_name'])) {
    return;
  }

  // Altering line item widget to add PSA options.
  if ($element['#field_name'] == 'commerce_line_items' && $element['#bundle'] == 'commerce_order' && isset($element['line_items'])) {
    foreach ($element['line_items'] as $line_item_id => $line_item) {
      $li = commerce_line_item_load($line_item_id);
      if ($li->type != 'product') {
        continue;
      }
      $lw = entity_metadata_wrapper('commerce_line_item', $li);
      $psa_options = $lw->psa_options->value();
      if (empty($psa_options)) {
        continue;
      }
      $title =& $element['line_items'][$line_item_id]['title'];
      $title[0]['#markup'] = $title['#markup'];
      $title[1]['#markup'] = theme('item_list', array(
        'items' => $psa_options,
      ));
      unset($title['#markup']);
    }
  }

  // Alter inline entity form widget to add description and remove
  // 'add new variation' button.
  if (isset($context['form']['#node_edit_form']) && $context['form']['#node_edit_form'] && isset($context['form']['#node'])) {
    module_load_include('inc', 'commerce_psa', 'includes/commerce_psa.common');
    $field_name = commerce_psa_product_reference_name($context['form']['#node']->type);

    if ($field_name && $element['#field_name'] == $field_name) {
      $element['actions']['ief_add']['#access'] = FALSE;
      $element['#description'] = t('Put default product on the top (under Source Product).');
    }
  }
}

/**
 * Implements hook_commerce_product_reference_default_delta_alter().
 */
function commerce_psa_commerce_product_reference_default_delta_alter(&$delta, $products) {
  $product = current($products);
  if (count($products) > 1) {
    module_load_include('inc', 'commerce_psa', 'includes/commerce_psa.common');
    $nid = commerce_psa_product_node($product->product_id, TRUE);
    $attributes = commerce_psa_node_attributes($nid);
    if ($nid && !empty($attributes)) {
      $new_delta = commerce_psa_products_default($products, count($attributes));
      $delta = $new_delta !== FALSE ? $new_delta : $delta;
    }
  }
}

/**
 * Find first product that have options enabled.
 *
 * @param array $products
 * @param int $attributes_count
 * @return boolean
 */
function commerce_psa_products_default($products = array(), $attributes_count = 1) {
  // In case this is Product Display node and it has Product Specific
  // Attributes, use second product as default (because first product is
  // the source product for PSA).
  foreach($products as $key => $product) {
    $options = commerce_psa_product_options_load($product->product_id);
    // Filter not appropriate products.
    if(empty($options) || count($options) != $attributes_count) {
      continue;
    }
    foreach($options as $option) {
      $option_values = commerce_psa_option_load($option['option_id'], NULL, array(
        array(
          'field' => 'enabled',
          'value' => 1,            
        )
      ));
      if(empty($option_values)) {
        continue 2;
      }
    }
    // Return key that matches all conditions.
    return $key;
  }
  return FALSE;
}

/**
 * Get product specific attributes for nid (node).
 *
 * @param $nid
 *   Node ID.
 * @param bool $filter
 *   Filter attributes and options with enabled ON.
 *
 * @return type
 */
function commerce_psa_node_attributes($nid, $filter = TRUE) {
  $filters = array();
  if ($filter) {
    array_push($filters, array(
      'field' => 'enabled',
      'value' => 1,
    ));
  }
  $attributes = commerce_psa_load(NULL, $nid, $filters);
  if (empty($attributes)) {
    return;
  }
  foreach ($attributes as $key => $attribute) {
    $attributes[$key]['options'] = commerce_psa_option_load(NULL, $attribute['psa_id'], $filters);
    if (empty($attributes[$key]['options'])) {
      unset($attributes[$key]);
    }
  }
  return $attributes;
}

/**
 * Implements hook_form_alter().
 */
function commerce_psa_form_alter(&$form, &$form_state, $form_id) {
  // Lets try to add PSA form elements to Commerce Add to Cart form.
  if (strstr($form_id, 'commerce_cart_add_to_cart_form')) {
    $nid = $form_state['context']['entity_id'];
    $attributes = commerce_psa_node_attributes($nid);
    
    $combination = & $form_state['values']['psa'];
    if (empty($combination)) {
      if (isset($form_state['default_product']->product_id)) {
        $default_options = commerce_psa_product_options($form_state['default_product']->product_id);
      }
    }

    if (!empty($attributes)) {
      foreach ($attributes as $attribute) {
        $options = array();

        if (empty($attribute['options'])) {
          continue;
        }
        foreach ($attribute['options'] as $value) {
          $options[$value['option_id']] = commerce_psa_i18n_string($value['name'], 'option');

          if (!empty($default_options) && array_key_exists($value['option_id'], $default_options)) {
            $combination[$attribute['psa_id']] = $value['option_id'];
          }
        }

        $form['psa']['#tree'] = TRUE;
        $form['psa'][$attribute['psa_id']] = array(
          '#type' => $attribute['field_type'],
          '#options' => $options,
          '#title' => commerce_psa_i18n_string($attribute['name']),
          '#default_value' => $combination[$attribute['psa_id']],
          '#ajax' => array(
            'callback' => 'commerce_cart_add_to_cart_form_attributes_refresh',
          ),
          '#attributes' => array(
            'class' => array('psa-options'),
          ),
        );
      }
    }

    $product_id = commerce_psa_combination_product_load($combination);
    if (!$product_id) {
      unset($form['psa']);
      $form['product_id']['#type'] = 'hidden';
      return;
    }
    $form_state['default_product'] = commerce_product_load($product_id);
    $form['product_id'] = array(
      '#type' => 'hidden',
      '#value' => $product_id,
    );
  }
}

/**
 * Implements hook_admin_paths().
 */
function commerce_psa_admin_paths() {
  // Define PSA paths as admin paths.
  return array(
    'node/*/psa' => TRUE,
    'node/*/psa/*' => TRUE,
  );
}

/**
 * Implements hook_entity_info_alter().
 */
function commerce_psa_entity_info_alter(&$entity_info) {
  if (isset($entity_info['commerce_product'])) {
    // Swap inline entity form default controller with ours.
    $entity_info['commerce_product']['inline entity form'] = array(
      'controller' => 'CommercePsaProductInlineEntityFormController',
    );
  }
}

/**
 * Implements hook_node_delete().
 */
function commerce_psa_node_delete($node) {
  if (!array_key_exists($node->type, commerce_product_reference_node_types())) {
    return;
  }

  $psa = commerce_psa_load(NULL, $node->nid);
  if (!empty($psa)) {
    $psa_ids = array_keys($psa);
    array_map('commerce_psa_delete', $psa_ids);
  }
}

/**
 * Implements hook_node_update().
 */
function commerce_psa_node_update($node) {
  if (!array_key_exists($node->type, commerce_product_reference_node_types())) {
    return;
  }

  $node_wrappper = entity_metadata_wrapper('node', $node);
  module_load_include('inc', 'commerce_psa', 'includes/commerce_psa.common');
  $reference_name = commerce_psa_product_reference_name($node->type);
  $first_product = commerce_psa_get_first_product($node);

  foreach ($node_wrappper->{$reference_name} as $product_wrapper) {
    $product = $product_wrapper->value();
    if ($first_product->product_id != $product->product_id) {
      commerce_psa_product_update($product, $node);
    }
  }
}

/**
 * Implements hook_commerce_line_item_presave().
 */
function commerce_psa_commerce_line_item_presave($line_item) {
  if ($line_item->type != 'product') {
    return;
  }

  $product_id = $line_item->commerce_product[LANGUAGE_NONE][0]['product_id'];
  module_load_include('inc', 'commerce_psa', 'includes/commerce_psa.common');
  $line_item->psa_options[LANGUAGE_NONE] = array();
  $options = commerce_psa_product_options_formatted($product_id);
  foreach($options as $option) {
    $line_item->psa_options[LANGUAGE_NONE][]['value'] = $option;
  }
}

/**
 * Implements hook_commerce_product_delete().
 */
function commerce_psa_commerce_product_delete($product) {
  db_delete('commerce_psa_linking')
      ->condition('product_id', $product->product_id)
      ->execute();
}

/**
 * Load Product Specific Attributes.
 *
 * @param int $psa_id
 *   PSA ID.
 * @param int $nid
 *   Node ID.
 * @param array $filters
 *   Query filters.
 * @param bool $reset
 *   Reset result or return cached.
 *
 * @return array
 *   PSA array.
 */
function commerce_psa_load($psa_id = NULL, $nid = NULL, array $filters = array(), $reset = FALSE) {
  $psa = &drupal_static(__FUNCTION__);
  $psa_nid = &drupal_static(__FUNCTION__ . ':nid');
  $psa_all = &drupal_static(__FUNCTION__ . ':all');

  $md5_id = md5(serialize(func_get_args()));

  $query = db_select('commerce_psa', 'p')
      ->fields('p')
      ->orderBy('p.weight');

  commerce_psa_option_load_filter($query, $filters);

  if (isset($psa_id)) {
    if (!isset($psa[$md5_id]) || $reset) {
      $psa[$md5_id] = $query
          ->condition('p.psa_id', $psa_id)
          ->execute()
          ->fetchAssoc();
    }
    return $psa[$md5_id];
  }
  elseif (isset($nid)) {
    if (!isset($psa_nid[$md5_id]) || $reset) {
      $psa_nid[$md5_id] = $query
          ->condition('p.nid', $nid)
          ->execute()
          ->fetchAllAssoc('psa_id', PDO::FETCH_ASSOC);
    }
    return $psa_nid[$md5_id];
  }
  else {
    if (!isset($psa_all) || $reset) {
      $psa_all = $query
          ->execute()
          ->fetchAllAssoc('psa_id', PDO::FETCH_ASSOC);
    }
    return $psa_all;
  }
}

/**
 * Load Product Attribute Options.
 *
 * @param int $option_id
 *   Option ID.
 * @param int $psa_id
 *   PSA ID.
 * @param array $filters
 *   Query filters.
 * @param bool $reset
 *   Reset result or return cached.
 *
 * @return array
 *   PSA option(s) array.
 */
function commerce_psa_option_load($option_id = NULL, $psa_id = NULL, array $filters = array(), $reset = FALSE) {
  $options = &drupal_static(__FUNCTION__);
  $options_psa = &drupal_static(__FUNCTION__);

  $md5_id = md5(serialize(func_get_args()));

  $query = db_select('commerce_psa_option', 'o')
      ->fields('o')
      ->orderBy('o.weight');

  commerce_psa_option_load_filter($query, $filters);

  if (isset($option_id)) {
    if (!isset($options[$md5_id]) || $reset) {
      $query->condition('o.option_id', $option_id);
      $options[$md5_id] = $query
          ->execute()
          ->fetchAssoc();
    }
    return $options[$md5_id];
  }
  elseif (isset($psa_id)) {
    if (!isset($options_psa[$md5_id]) || $reset) {
      $query->condition('o.psa_id', $psa_id);
      $options_psa[$md5_id] = $query
          ->execute()
          ->fetchAllAssoc('option_id', PDO::FETCH_ASSOC);
    }

    return $options_psa[$md5_id];
  }
}

/**
 * Convert filters array to query object.
 *
 * @param object $query
 *   Query object.
 * @param array $filters
 *   Filters.
 */
function commerce_psa_option_load_filter($query, array $filters) {
  foreach ($filters as $filter) {
    $query->condition($filter['field'], $filter['value']);
  }
}

/**
 * Get product's PSA option key => name pairs.
 *
 * @param int $product_id
 *   Product ID.
 *
 * @return array
 *   Option array.
 */
function commerce_psa_product_options($product_id) {
  $options = array();
  foreach (commerce_psa_product_options_load($product_id) as $option) {
    $option = commerce_psa_option_load($option['option_id']);
    $options[$option['option_id']] = $option['name'];
  }
  return $options;
}

/**
 * Load product psa options.
 *
 * @param int $product_id
 *   Product ID.
 */
function commerce_psa_product_options_load($product_id) {
  static $options = array();
  if (!isset($options[$product_id])) {
    $options[$product_id] = db_select('commerce_psa_linking', 'l')
        ->fields('l', array('option_id'))
        ->condition('l.product_id', $product_id)
        ->execute()
        ->fetchAll(PDO::FETCH_ASSOC);
  }
  return $options[$product_id];
}

/**
 * Removes PSA.
 *
 * @param int $psa_id
 *   PSA ID.
 */
function commerce_psa_delete($psa_id) {
  $options = commerce_psa_option_load(NULL, $psa_id);
  foreach($options as $option) {
    if (!commerce_psa_option_delete($option['option_id'])) {
      $dont_remove = TRUE;
    }
  }
  if (isset($dont_remove)) {
    drupal_set_message(t('Product attribute cannot be removed because it consist of the product options which are linked with already purchased product(s). Still, you have ability to simply disable it in order to hide it at front-end. Please, refresh the page.'), 'warning');
    return FALSE;
  } else {
    db_delete('commerce_psa')
        ->condition('psa_id', $psa_id)
        ->execute();
    return TRUE;
  }
}

/**
 * Removed PSA Option.
 *
 * @param int $option_id
 *   PSA options ID.
 */
function commerce_psa_option_delete($option_id) {
  $products = db_select('commerce_psa_linking', 'l')
      ->fields('l', array(
        'product_id',
      ))
      ->condition('option_id', $option_id)
      ->execute()
      ->fetchAllAssoc('product_id', PDO::FETCH_ASSOC);

  $can_remove = TRUE;
  foreach ($products as $product) {
    if (!commerce_product_delete($product['product_id'])) {
      $can_remove = FALSE;
      break;
    }
  }

  if($can_remove) {
    db_delete('commerce_psa_option')
        ->condition('option_id', $option_id)
        ->execute();
    db_delete('commerce_psa_linking')
        ->condition('option_id', $option_id)
        ->execute();
  } else {
    drupal_set_message(t('Product attribute option cannot be removed because it linked with already purchased product(s). Still, you have ability to simply disable it in order to hide it at front-end. Please, refresh the page.'), 'warning');
  }
  return $can_remove;
}

/**
 * Get product ID by conbination of PSA options.
 *
 * @param mixed $combination
 *   Options combination.
 */
function commerce_psa_combination_product_load($combination = array()) {
  if (empty($combination)) {
    return;
  }
  $query = db_select('commerce_product', 'p');
  foreach ($combination as $options_id) {
    $alias = sprintf('l%d', $options_id);
    $query->innerJoin('commerce_psa_linking', $alias, "p.product_id = $alias.product_id");
    $query->condition("$alias.option_id", $options_id);
  }
  return $query
          ->fields('p', array('product_id'))
          ->execute()
          ->fetchColumn();
}

/**
 * Implements hook_i18n_string_info().
 */
function commerce_psa_i18n_string_info() {
  $groups['commerce_psa'] = array(
    'title' => t('Drupal Commerce PSA'),
    'format' => TRUE,
  );
  return $groups;
}

/**
 * Commerce i18n string wrapper specifically for PSA.
 *
 * @param string $string
 *   String to tranlsate/update.
 * @param string $type
 *   Use `attribute` or `option`.
 * @param bool $update
 *   Pass TRUE to localize variable.
 *
 * @return string
 *   Possibly translated string.
 */
function commerce_psa_i18n_string($string, $type = 'attribute', $update = FALSE) {
  $i18n_id = 'commerce_psa:' . $type . ':' . md5($string);
  $string = commerce_i18n_string($i18n_id, $string, array(
    'update' => $update,
  ));
  return check_plain($string);
}
