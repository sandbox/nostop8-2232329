<?php

/**
 * @file
 * Commerce PSA hooks.
 */

/**
 * Alter Commerce PSA product title.
 *
 * @param string $title
 *   Title to alter.
 * @param object $node
 *   Current node.
 */
function hook_commerce_psa_product_title_alter(&$title, &$node) {
  $title = $node->field_title[LANGUAGE_NONE][0]['value'];
}
