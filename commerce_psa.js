/**
 * @file
 *
 * Commerce PSA javascript functionality: open/close PSA change options dialog.
 */

(function($) {
    Drupal.ajax = Drupal.ajax || {};
    Drupal.ajax.prototype.commands.commerce_psa_change_options_refresh = function(ajax, response, status) {
        if (status !== 'success') {
            return;
        }
        if (response.data) {
            $('.psa-dialog').dialog('close');
            window.location.reload();
        }
    };

    Drupal.behaviors.commerce_psa = {
        attach: function(c) {
            if ($(c).parent().length) {
                c = $(c).parent().get(0);
            }

            $('.psa-dialog', c).once(function() {
                $(this).removeClass('element-invisible');
                $(this).dialog({
                    title: $('h2.title', this).text(),
                    modal: true,
                    minWidth: 420,
                    maxHeight: 600,
                    close: function() {
                        $(this).remove();
                    },
                    show: {
                        effect: "fadeIn",
                        duration: 300
                    },
                    hide: {
                        effect: "fadeOut",
                        duration: 300
                    }
                });
            });
        }
    };
})(jQuery);
