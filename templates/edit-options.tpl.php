<?php
/**
 * @file
 * PSA edit options form template.
 */

?>
<div class="element-invisible psa-dialog">
  <h2 class="title element-invisible"><?php print t('Change Options') ?></h2>
  <?php print drupal_render($form) ?>
</div>
