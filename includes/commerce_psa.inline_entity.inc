<?php

/**
 * @file
 * Product inline entity form controller (extending default).
 */

class CommercePsaProductInlineEntityFormController extends CommerceProductInlineEntityFormController {

  /**
   * Overrides CommerceProductInlineEntityFormController::tableFields().
   */
  public function tableFields($bundles) {
    $fields = parent::tableFields($bundles);
    $fields['product_id'] = array(
      'type' => 'property',
      'label' => 'Product ID',
      'weight' => 2,
    );
    $fields['combination'] = array(
      'type' => 'psa',
      'label' => t('Combination'),
      'weight' => 1,
    );

    if (module_exists('commerce_stock')) {
      $fields['commerce_stock'] = array(
        'type' => 'field',
        'label' => t('Stock'),
        'weight' => 4,
      );
    }

    unset($fields['title']);
    return $fields;
  }

  /**
   * Overrides CommerceProductInlineEntityFormController::attributes().
   */
  protected function attributes($type) {
    $attrs = parent::attributes($type);
    return $attrs;
  }

}
