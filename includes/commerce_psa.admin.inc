<?php

/**
 * @file
 * Admin area forms to manage Product Specific Attributes and its options.
 */

class CommercePsa {

  protected $type = 'attribute';
  protected $dbTable = 'commerce_psa';
  protected $dbPk = 'psa_id';
  protected $dbParentPk = 'nid';
  protected $node;
  protected $form = array('#tree' => TRUE);
  protected $values;
  protected $table = array('#theme' => 'psa_table');
  protected $formState;

  /**
   * Singltone implementation of class.
   */
  public static function getInstance(&$form_state = NULL) {
    static $instance = NULL;
    if ($instance === NULL) {
      $instance = new static();
      $args = func_get_args();
      // Assign form state to object's formState property in order
      // to be able to use it as a reference later.
      $instance->formState = & $form_state;
      // Remove &form_state from $args, as it is as object property already.
      array_shift($args);
      call_user_func_array(array($instance, 'buildForm'), $args);
    }
    return $instance;
  }

  /**
   * Providing form elements.
   */
  protected function buildForm() {
    $args = func_get_args();
    $this->node = $args[0];

    $this->values = $this->loadValues();
    array_walk($this->values, array($this, 'formElement'));

    $this->table['#form_class'] = get_class($this);
    $this->form['table'] = $this->table;

    $ajax = array(
      'wrapper' => 'psa-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
      'callback' => sprintf('commerce_psa_%s_form_callback', $this->type),
      'effect' => 'none',
    );

    $this->form['actions']['add'] = array(
      '#name' => 'add',
      '#type' => 'submit',
      '#value' => t('Add'),
      '#ajax' => $ajax,
    );

    $this->form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#name' => 'save',
      '#ajax' => $ajax,
    );

    $this->form['nid'] = array(
      '#type' => 'value',
      '#value' => $this->node->nid,
    );
  }

  /**
   * Returns form.
   */
  public function getForm() {
    $this->form['title'] = array(
      '#type' => 'markup',
      '#markup' => '<h2>Manage Attributes</h2>',
      '#weight' => -100,
    );
    return $this->form;
  }

  /**
   * Load default values.
   */
  protected function loadValues() {
    $values = array();
    $form_state = & $this->formState;
    if (!empty($form_state['storage']['table'])) {
      $values = $form_state['storage']['table'];
    }
    if (empty($form_state['values'])) {
      $values = $this->load();
      $form_state['storage']['table'] = $values;

      if (empty($values)) {
        $values = array($this->emptyRow());
      }
    }
    return $values;
  }

  /**
   * Form elements.
   *
   * @param mixed $value
   *   Value.
   * @param int $i
   *   Iterator number.
   */
  protected function formElement($value, $i) {
    $table = &$this->table;

    $table[$i]['psa_id'] = array(
      '#type' => 'hidden',
      '#value' => $value['psa_id'],
    );
    $table[$i]['nid'] = array(
      '#type' => 'hidden',
      '#value' => $this->node->nid,
    );
    $table[$i]['name'] = array(
      '#type' => 'textfield',
      '#default_value' => $value['name'],
      '#required' => TRUE,
    );
    $table[$i]['enabled'] = array(
      '#type' => 'checkbox',
      '#default_value' => $value['enabled'],
    );
    $table[$i]['field_type'] = array(
      '#type' => 'select',
      '#options' => array(
        'radios' => t('Radios'),
        'select' => t('Select'),
      ),
      '#default_value' => isset($value['field_type']) ? $value['field_type'] : NULL,
    );
    if (!isset($value['weight'])) {
      $weight = db_select($this->dbTable, 't')
          ->fields('t', array('weight'))
          ->condition($this->dbParentPk, $this->node->nid)
          ->orderBy('weight', 'DESC')
          ->range(0, 1)
          ->execute()
          ->fetchColumn();

      $value['weight'] = $weight + 1;
    }
    $table[$i]['weight'] = array(
      '#type' => 'textfield',
      '#default_value' => $value['weight'],
      '#size' => 3,
      '#attributes' => array(
        'class' => array('weight'),
      ),
    );
    $table[$i]['remove'] = array(
      '#name' => 'remove' . $i,
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#attributes' => array(
        'data-number' => $i,
      ),
      '#ajax' => array(
        'wrapper' => 'psa-wrapper',
        'method' => 'replace',
        'callback' => sprintf('commerce_psa_%s_form_callback', $this->type),
        'effect' => 'none',
      ),
      '#limit_validation_errors' => array(),
      '#submit' => array(sprintf('commerce_psa_%s_form_submit', $this->type)),
    );
  }

  /**
   * Allowing to alter table output.
   */
  protected function themeTableAlter(&$rows, &$header) {}

  /**
   * Theme table.
   */
  public function themeTable($table) {
    drupal_add_tabledrag('psa-table', 'order', 'sibling', 'weight');

    $header = array(
      'name' => array('data' => t('Name')),
      'field_type' => array('data' => t('Field Type')),
      'enabled' => array('data' => t('Enabled')),
      'weight' => array('data' => t('Weight')),
      'actions' => array('data' => t('Actions')),
    );

    foreach (element_children($table) as $row_key) {
      $row = $this->themeRow($table[$row_key]);
      $row['class'] = array('draggable');
      $rows[] = $row;
    }

    $this->themeTableAlter($rows, $header);

    $table = theme('table', array(
      'rows' => $rows,
      'header' => $header,
      'attributes' => array(
        'id' => 'psa-table',
      ),
    ));
    $messages = theme('status_messages');
    return sprintf('<div id="psa-wrapper">%s%s</div>', $messages, $table);
  }

  /**
   * Row theming.
   */
  protected function themeRow($element, $row = array()) {
    $row['data'][] = render($element['name']) . render($element['psa_id']) . render($element['nid']);
    $row['data'][] = render($element['field_type']);
    $row['data'][] = render($element['enabled']);
    $row['data'][] = render($element['weight']);

    $actions = render($element['remove']);
    if ($element['psa_id']['#value']) {
      $actions .= l(t('Manage Options'), sprintf('node/%d/psa/%d', $this->node->nid, $element['psa_id']['#value']));
    }
    $row['data'][] = $actions;
    return $row;
  }

  /**
   * Starts generate products per PSA.
   */
  protected function submitGenerate(&$form_state, $records, $remove_items) {
    $node = node_load($form_state['values']['nid']);
    module_load_include('inc', 'commerce_psa', 'includes/commerce_psa.common');
    commerce_psa_generate_products($node);
  }

  /**
   * Removes element.
   */
  protected function submitRemove(&$form_state, &$records, &$remove_items) {
    $number = $form_state['clicked_button']['#attributes']['data-number'];
    if ($records[$number][$this->dbPk]) {
      $remove_items[] = $records[$number][$this->dbPk];
    }
    unset($records[$number]);
  }

  /**
   * Save PSA.
   */
  protected function submitSave(&$form_state, &$records, &$remove_items) {
    // Updating and inserting values.
    $first_record = !empty($records) ? current($records) : array();
    if (count($records) > 1 || (count($records) == 1 && isset($first_record['name']) && $first_record['name'])) {
      $records = array_map(array($this, 'save'), $records);
    }
    // Removing values.
    array_map(array($this, 'remove'), $remove_items);
    $remove_items = array();
    drupal_set_message(t('Product Attributes has been saved.'));
    // Generate all possible product variables.
    $this->submitGenerate($form_state, $records, $remove_items);
  }

  /**
   * Submit form.
   */
  public function submit(&$form_state) {
    $is_remove = strpos($form_state['clicked_button']['#name'], 'remove') === 0;
    // In case this is not remove button clicked, lets save to the storage
    // our values. $form_state['values'] won't provide all submitted form
    // values because of #limit_validation_errors.
    if (!$is_remove) {
      $records = $form_state['values']['table'];
      $form_state['storage']['table'] = & $records;
    }
    else {
      $records = & $form_state['storage']['table'];
    }
    $remove_items = & $form_state['storage']['remove_items'];
    if ($remove_items === NULL) {
      $remove_items = array();
    }

    switch ($form_state['clicked_button']['#name']) {
      // Add.
      case 'add':
        $records[] = $this->emptyRow();
        break;

      // Remove.
      case ($is_remove):
        $this->submitRemove($form_state, $records, $remove_items);
        break;

      // SAVE.
      default:
        $this->submitSave($form_state, $records, $remove_items);
        // Clear form state input on save, as it provides incorrect default
        // values.
        break;
    }

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Remove specific element.
   *
   * @param int $id
   *   PSA ID.
   */
  protected function remove($id) {
    return commerce_psa_delete($id);
  }

  /**
   * Save specific element.
   *
   * @param object $record
   *   PSA record.
   */
  protected function save($record) {
    $record = (object) $record;
    if (!isset($record->{$this->dbPk}) || !$record->{$this->dbPk}) {
      drupal_write_record($this->dbTable, $record);
    }
    else {
      drupal_write_record($this->dbTable, $record, array($this->dbPk));
    }
    commerce_psa_i18n_string($record->name, $this->type, TRUE);
    return (array) $record;
  }

  /**
   * Load PSA per node.
   *
   * @return array
   *   Attributes of node.
   */
  protected function load() {
    return commerce_psa_load(NULL, $this->node->nid);
  }

  /**
   * Default empty row values.
   *
   * @return array
   *   Default empty row values.
   */
  protected function emptyRow() {
    return array(
      'psa_id' => NULL,
      'name' => '',
      'enabled' => TRUE,
      'field_type' => 'radios',
      'weight' => NULL,
    );
  }

}

class CommercePsaOptions extends CommercePsa {

  public $psa = array();

  /**
   * Overrides magic method CommercePsa::__construct.
   */
  public function __construct() {
    $this->dbTable = 'commerce_psa_option';
    $this->dbPk = 'option_id';
    $this->dbParentPk = 'psa_id';
    $this->type = 'option';
  }

  /**
   * Overrides method CommercePsa::buildForm.
   */
  protected function buildForm() {
    $args = func_get_args();
    $this->psa = $args[1];
    call_user_func_array(array('parent', 'buildForm'), $args);
  }

  /**
   * Overrides method CommercePsa::getForm.
   */
  public function getForm() {
    $form['title'] = array(
      '#type' => 'markup',
      '#markup' => sprintf('<h2>%s</h2>', $this->psa['name']),
    );
    $form += parent::getForm();
    $form['actions']['back'] = array(
      '#type' => 'link',
      '#href' => sprintf('node/%d/psa', arg(1)),
      '#title' => '« Back to Attributes',
    );
    unset($form['actions']['generate']);
    return $form;
  }

  /**
   * Overrides method CommercePsa::formElement.
   */
  protected function formElement($value, $i) {
    parent::formElement($value, $i);
    $table = &$this->table;

    $table[$i]['psa_id'] = array(
      '#type' => 'hidden',
      '#value' => $this->psa['psa_id'],
    );
    $table[$i]['option_id'] = array(
      '#type' => 'hidden',
      '#value' => $value['option_id'],
    );
    unset($table['nid']);
    unset($table['field_type']);
  }

  /**
   * Overrides method CommercePsa::themeTableAlter.
   */
  protected function themeTableAlter(&$rows, &$header) {
    unset($header['field_type']);
  }

  /**
   * Overrides method CommercePsa::themeRow.
   */
  protected function themeRow($element, $row = array()) {
    $row['data'][] = render($element['name']) . render($element['option_id']) . render($element['psa_id']);
    $row['data'][] = render($element['enabled']);
    $row['data'][] = render($element['weight']);
    $row['data'][] = render($element['remove']);
    return $row;
  }

  /**
   * Overrides method CommercePsa::load.
   */
  protected function load() {
    return commerce_psa_option_load(NULL, $this->psa['psa_id']);
  }

  /**
   * Overrides method CommercePsa::remove.
   */
  protected function remove($id) {
    return commerce_psa_option_delete($id);
  }

  /**
   * Overrides method CommercePsa::emptyRow.
   */
  protected function emptyRow() {
    return array(
      'psa_id' => NULL,
      'option_id' => NULL,
      'name' => '',
      'enabled' => TRUE,
      'weight' => NULL,
    );
  }

}

/**
 * Attributes form.
 *
 * @param object $node
 *   Node object.
 */
function commerce_psa_attribute_form($form = NULL, &$form_state = NULL, $node = NULL) {
  $breadcrumbs = drupal_get_breadcrumb();
  $breadcrumbs[] = t('Product Attributes');
  drupal_set_breadcrumb($breadcrumbs);
  $form = CommercePsa::getInstance($form_state, $node);
  return $form->getForm();
}

/**
 * Ajax callback.
 *
 * @return string
 *   Rendered table (html).
 */
function commerce_psa_attribute_form_callback($form, $form_state) {
  return render($form['table']);
}

/**
 * Submit callback.
 */
function commerce_psa_attribute_form_submit($form, &$form_state) {
  $form = new CommercePsa();
  $form->submit($form_state);
}

/**
 * Options form.
 *
 * @param object $node
 *   Node object.
 * @param array $psa
 *   PSA array.
 *
 * @return array
 *   Form array.
 */
function commerce_psa_option_form($form = NULL, &$form_state = NULL, $node = NULL, array $psa = array()) {
  $breadcrumbs = drupal_get_breadcrumb();
  $breadcrumbs[] = l(t('Product Attributes'), "node/$node->nid/psa");
  $breadcrumbs[] = $psa['name'];
  drupal_set_breadcrumb($breadcrumbs);
  $form = CommercePsaOptions::getInstance($form_state, $node, $psa);
  return $form->getForm();
}

/**
 * Options ajax callback.
 *
 * @return string
 *   Rendered table (html).
 */
function commerce_psa_option_form_callback($form, $form_state) {
  return render($form['table']);
}

/**
 * Options form submit callback.
 */
function commerce_psa_option_form_submit($form, &$form_state) {
  $form = new CommercePsaOptions();
  $form->submit($form_state);
}
