<?php

/**
 * @file
 * PSA Views handler(s).
 */

/**
 * Implements hook_views_data().
 */
function commerce_psa_views_data() {
  $data = array();
  // Adds a button to edit line item PSA options.
  $data['commerce_line_item']['edit_options'] = array(
    'field' => array(
      'title' => t('Edit PSA Options'),
      'help' => t('Adds a button to edit PSA options of the cart`s product(s).'),
      'handler' => 'commerce_psa_line_item_handler_field_edit_options',
    ),
  );
  return $data;
}
