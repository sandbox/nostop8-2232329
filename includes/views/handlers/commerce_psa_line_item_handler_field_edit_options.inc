<?php

/**
 * @file
 * Edit PSA options link views handler.
 */

class commerce_psa_line_item_handler_field_edit_options extends views_handler_field {

  /**
   * Overriding method views_handler_field::construct.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['line_item_id'] = 'line_item_id';

    // Set real_field in order to make it generate a field_alias.
    $this->real_field = 'line_item_id';
  }

  /**
   * Returns placeholder of the field.
   */
  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    if (empty($this->view->result)) {
      return;
    }

    drupal_add_library('system', 'ui.dialog');
    drupal_add_library('system', 'drupal.ajax');
    drupal_add_js(drupal_get_path('module', 'commerce_psa') . '/commerce_psa.js');

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );

    foreach ($this->view->result as $row_id => $row) {
      $line_item_id = $this->get_value($row);
      $line_item = commerce_line_item_load($line_item_id);
      module_load_include('inc', 'commerce_psa', 'includes/commerce_psa.pages');
      $attributes = _commerce_psa_line_item_attributes($line_item);
      if (empty($attributes)) {
        continue;
      }

      $form[$this->options['id']][$row_id] = array(
        '#type' => 'link',
        '#title' => t('Edit Options'),
        '#href' => "psa/nojs/$line_item_id",
        '#attributes' => array('class' => array('psa-edit-options use-ajax form-submit')),
      );
    }
  }

}
