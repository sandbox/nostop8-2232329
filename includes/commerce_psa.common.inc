<?php

/**
 * @file
 * PSA common functions.
 */

/**
 * Generate all possible products for PSA.
 */
function commerce_psa_generate_products($node) {
  $node = clone $node;

  $attributes = commerce_psa_node_attributes($node->nid, FALSE);
  $combinations = commerce_psa_generate_products_combine($attributes);

  $reference_name = commerce_psa_product_reference_name($node->type);
  $items = field_get_items('node', $node, $reference_name);
  $first_product = commerce_psa_get_first_product($node);

  $products_remove = array();
  foreach ($items as $item) {
    if ($item['product_id'] == $first_product->product_id) {
      continue;
    }
    $products_remove[$item['product_id']] = $item['product_id'];
  }

  foreach ($combinations as $combination) {
    $product_id = commerce_psa_combination_product_load($combination);

    if ($product_id) {
      if (isset($products_remove[$product_id])) {
        unset($products_remove[$product_id]);
      } else {
        // Re-attach product to the node, in case it has gone somehow.
        $node->{$reference_name}[LANGUAGE_NONE][] = array(
          'product_id' => $product_id,
        );
      }
      $product = commerce_product_load($product_id);
      commerce_psa_product_update($product, $node);
      continue;
    }

    $product = commerce_psa_product_create($node, $combination);

    if (!isset($product->product_id) || !$product->product_id) {
      drupal_set_message(t('Error generating product variant. Interrupting the process.'), 'error');
      return;
    }

    $node->{$reference_name}[LANGUAGE_NONE][] = array(
      'product_id' => $product->product_id,
    );
    commerce_psa_save_combination_link($product->product_id, $combination);
    drupal_set_message(sprintf('Product variant %s has been generated successfully.', $product->sku));
  }

  // Removing products and its references.
  $field_items = array();
  foreach ($node->{$reference_name}[LANGUAGE_NONE] as $key => $item) {
    $field_items[$item['product_id']] = $key;
  }
  foreach ($products_remove as $product_id) {
    if (!commerce_product_delete($product_id)) {
      $product = commerce_product_load($product_id);
      drupal_set_message(t('Note: product variant !product_id (SKU: !sku) was not removed from the system, because it is already purchased.', array(
        '!product_id' => $product_id,
        '!sku' => $product->sku)), 'warning');
    } else {
      unset($node->{$reference_name}[LANGUAGE_NONE][$field_items[$product_id]]);
    }
  }

  node_save($node);
}

/**
 * Recursive function to build all possible combinations of attribute options.
 *
 * @return array
 *   All possible combinations.
 */
function commerce_psa_generate_products_combine($groups = array(), $reset = TRUE, $income_combination = array()) {
  $groups = array_values($groups);
  static $combinations = array();
  if ($reset) {
    $combinations = array();
  }
  $current_group = current($groups);
  foreach ($current_group['options'] as $option) {
    $combination = $income_combination;
    $combination[] = $option['option_id'];
    if (count($groups) === 1) {
      $combinations[] = $combination;
    }
    else {
      commerce_psa_generate_products_combine(array_slice($groups, 1), FALSE, $combination);
    }
  }
  return $combinations;
}

/**
 * Link options with generated product.
 *
 * @param int $product_id
 *   Product ID.
 * @param array $combination
 *   PSA Combination.
 */
function commerce_psa_save_combination_link($product_id, array $combination) {
  foreach ($combination as $option_id) {
    db_insert('commerce_psa_linking')->fields(array(
      'option_id' => $option_id,
      'product_id' => $product_id,
    ))->execute();
  }
}

/**
 * Create product based on source (first) node product and specific combination.
 *
 * @param object $node
 *   Node object.
 * @param array $combination
 *   Specific PSA combination.
 *
 * @return object
 *   Created product object.
 */
function commerce_psa_product_create($node, array $combination) {
  global $user;
  $first_product = commerce_psa_get_first_product($node);

  $product = commerce_product_new($node->type);
  $product->title = commerce_psa_product_title($node);
  $product->sku = sprintf('%s-%s', $first_product->sku, implode('', $combination));
  $product->uid = $user->uid;
  $product->created = $product->changed = time();
  $product->commerce_price = $first_product->commerce_price;
  $product->commerce_stock = $first_product->commerce_stock;
  $product->language = LANGUAGE_NONE;
  commerce_product_save($product);

  return $product;
}

/**
 * Update product.
 *
 * @param object $product
 *   Product object.
 * @param object $node
 *   Node object.
 */
function commerce_psa_product_update($product, $node) {
  $product->title = commerce_psa_product_title($node);
  $product->changed = time();
  commerce_product_save($product);
}

/**
 * Get node first product.
 *
 * @param object $node
 *   Node object.
 *
 * @return object
 *   Product object.
 */
function commerce_psa_get_first_product($node) {
  $items = field_get_items('node', $node, commerce_psa_product_reference_name($node->type));

  $first_product_item = current($items);
  $first_product = entity_load_single('commerce_product', $first_product_item['product_id']);

  return $first_product;
}

/**
 * Find product reference field name of specific node type.
 *
 * @param string $type
 *   Node type.
 *
 * @return string
 *   Product reference field name.
 */
function commerce_psa_product_reference_name($type) {
  foreach (field_info_field_map() as $field_name => $field_stub) {
    if ($field_stub['type'] == 'commerce_product_reference' && !empty($field_stub['bundles']['node'])) {
      if (in_array($type, $field_stub['bundles']['node'])) {
        return $field_name;
      }
    }
  }
}

/**
 * Get title for generated product from $node->title.
 *
 * Lets other modules to override the product title.
 *
 * @param object $node
 *   Node object.
 *
 * @return string
 *   Title.
 */
function commerce_psa_product_title($node) {
  $title = $node->title;
  drupal_alter('commerce_psa_product_title', $title, $node);
  return $title;
}

/**
 * Loading node by product ID.
 *
 * @param int $product_id
 *   Product ID.
 */
function commerce_psa_product_node($product_id, $only_nid = FALSE) {
  $cache = &drupal_static(__FUNCTION__);
  if(!isset($cache[$product_id])) {
    $bundles = commerce_product_reference_node_types();
    foreach ($bundles as $bundle) {
      $field_name = commerce_psa_product_reference_name($bundle->type);
      $db_table = sprintf('field_data_%s', $field_name);
      $db_column = sprintf('%s_product_id', $field_name);
      $nid = db_select($db_table, 'f')
          ->fields('f', array('entity_id'))
          ->condition('entity_type', 'node')
          ->condition('bundle', $bundle->type)
          ->condition($db_column, $product_id)
          ->execute()
          ->fetchColumn();

      if ($nid) {
        $cache[$product_id] = $nid;
        break;
      }
    }
  }
  if ($only_nid) {
    return $cache[$product_id];
  }
  return node_load($cache[$product_id]);
}

/**
 * Do product's formatted options list (array).
 *
 * @param int $product_id
 *   Product ID.
 *
 * @return array
 */
function commerce_psa_product_options_formatted($product_id) {
  $options = commerce_psa_product_options($product_id);
  if (empty($options)) {
    return array();
  }
  $psa_options = array();
  foreach ($options as $option_id => $option_name) {
    $option_data = commerce_psa_option_load($option_id);
    $attribute = commerce_psa_load($option_data['psa_id']);
    $i18n_attribute = commerce_psa_i18n_string($attribute['name']);
    $i18n_option = commerce_psa_i18n_string($option_name, 'option');
    $psa_options[] = sprintf('%s: %s', $i18n_attribute, $i18n_option);
  }
  return $psa_options;
}
