<?php

/**
 * @file
 * PSA front-end pages (edit options form).
 */

/**
 * Edit options menu callback.
 *
 * @param string $ajax
 *   AJAX indicator.
 * @param object $line_item
 *   Commerce line item.
 *
 * @return array
 *   Ajax commands or form array.
 */
function commerce_psa_edit_options($ajax, $line_item = NULL) {
  $form = drupal_get_form('commerce_psa_edit_options_form', $line_item);
  if ($ajax === 'ajax') {
    $commands = array();
    $commands[] = array(
      'command' => 'insert',
      'data' => theme('psa_edit_options', array('form' => $form)),
      'method' => 'append',
      'selector' => 'body',
    );
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  return $form;
}

/**
 * Edit options form.
 *
 * @param object $line_item
 *   Commerce line item.
 */
function commerce_psa_edit_options_form($form, $form_state, $line_item = NULL) {
  $form['#prefix'] = '<div id="edit-options-wrapper">';
  $form['#suffix'] = '</div>';
  $form['#action'] = '';

  $default_product_id = NULL;
  // Looking for product attributes.
  $attributes = _commerce_psa_line_item_attributes($line_item, $default_product_id);
  if (empty($attributes)) {
    return;
  }

  // Getting line item selected options.
  $combination = & $form_state['values']['psa'];
  if (empty($combination)) {
    $default_options = commerce_psa_product_options($default_product_id);
  }

  foreach ($attributes as $attribute) {
    $options = array();

    if (empty($attribute['options'])) {
      continue;
    }
    foreach ($attribute['options'] as $value) {
      $options[$value['option_id']] = commerce_psa_i18n_string($value['name'], 'option');
      if (!empty($default_options) && array_key_exists($value['option_id'], $default_options)) {
        $combination[$attribute['psa_id']] = $value['option_id'];
      }
    }

    $form['psa']['#tree'] = TRUE;
    $form['psa'][$attribute['psa_id']] = array(
      '#type' => $attribute['field_type'],
      '#options' => $options,
      '#title' => commerce_psa_i18n_string($attribute['name']),
      '#default_value' => $combination[$attribute['psa_id']],
      '#ajax' => array(
        'callback' => 'commerce_psa_edit_options_form_ajax_refresh',
        'wrapper' => 'edit-options-wrapper',
        'method' => 'replace',
      ),
    );
  }

  $form['line_item_id'] = array(
    '#type' => 'value',
    '#value' => $line_item->line_item_id,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  // Get product by PSA combination.
  $product_id = commerce_psa_combination_product_load($combination);
  $product = commerce_product_load($product_id);
  $product_wrapper = entity_metadata_wrapper('commerce_product', $product);

  $instance = field_read_instance('commerce_product', 'commerce_price', 'event');
  $display = field_get_display($instance, 'default', $product);
  $price = field_view_field('commerce_product', $product, 'commerce_price', $display);

  $form['actions']['price'] = array(
    '#markup' => drupal_render($price),
  );

  $out_of_stock = FALSE;
  if (module_exists('commerce_stock') && isset($product_wrapper->commerce_stock)) {
    if (!(isset($product_wrapper->commerce_stock_override) && $product_wrapper->commerce_stock_override->value() == 1)) {
      if ($product_wrapper->commerce_stock->value() <= 0) {
        $form['actions']['submit']['#value'] = t('Out of Stock');
        $form['actions']['submit']['#disabled'] = TRUE;
        $form['actions']['submit']['#attributes'] = array('class' => array('out-of-stock'));
        $out_of_stock = TRUE;
      }
    }
  }

  if (!$out_of_stock) {
    $form['actions']['submit']['#ajax'] = array(
      'callback' => 'commerce_psa_edit_options_form_ajax_submit',
    );
  }

  return $form;
}

/**
 * Options refresh ajax callback.
 */
function commerce_psa_edit_options_form_ajax_refresh($form, &$form_state) {
  return $form;
}

/**
 * Form ajax submit callback.
 *
 * @return array
 *   Ajax commands.
 */
function commerce_psa_edit_options_form_ajax_submit($form, &$form_state) {
  commerce_psa_edit_options_form_submit($form, $form_state);
  $commands = array();
  $commands[] = array(
    'command' => 'commerce_psa_change_options_refresh',
    'data' => TRUE,
  );
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Edit options submit callback.
 */
function commerce_psa_edit_options_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  // Load the line item and update it.
  $line_item = commerce_line_item_load($values['line_item_id']);

  // Get product by PSA combination.
  $product_id = commerce_psa_combination_product_load($values['psa']);
  $product = commerce_product_load($product_id);

  // Update line item.
  commerce_product_line_item_populate($line_item, $product);
  commerce_line_item_save($line_item);
  entity_get_controller('commerce_line_item')->resetCache(array($line_item->line_item_id));
}

/**
 * Get Attributes for line item.
 *
 * @param object $line_item
 *   Commerce line item.
 * @param int $product_id
 *   Commerce product ID.
 *
 * @return array
 *   PSA attributes.
 */
function _commerce_psa_line_item_attributes($line_item, &$product_id = NULL) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $product_id = $line_item_wrapper->commerce_product->getIdentifier();
  module_load_include('inc', 'commerce_psa', 'includes/commerce_psa.common');
  $node = commerce_psa_product_node($product_id);
  return commerce_psa_node_attributes($node->nid);
}
